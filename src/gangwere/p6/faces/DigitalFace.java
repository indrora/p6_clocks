package gangwere.p6.faces;

import gangwere.p6.IClockFace;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;

public class DigitalFace extends JPanel implements IClockFace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3961412172330100677L;
	JLabel l = new JLabel();
	
	public DigitalFace()
	{
		l.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 24));
		l.setText("[NO TEXT]\r\n");
		this.add(l);
	}
	
	@Override
	public void setTime(int hours, int minutes, int seconds) {
		// TODO Auto-generated method stub
		l.setText(String.format("%02d.%02d.%02d", hours, minutes,seconds));

	}

	@Override
	public void forceRefresh() {
		l.repaint();

	}

	@Override
	public JPanel getContentPanel() {
		// TODO Auto-generated method stub
		return this;
	}

}
