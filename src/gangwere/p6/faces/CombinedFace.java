/**
 * 
 */
package gangwere.p6.faces;

import java.awt.GridLayout;

import gangwere.p6.IClockFace;

import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * @author indrora
 *
 */
public class CombinedFace extends JPanel implements IClockFace {

	/**
	 * Wink at the moon!
	 */
	private static final long serialVersionUID = -1670772572322747728L;
	DigitalFace dface = new DigitalFace();
	AnalogFace aface = new AnalogFace();
	
	public CombinedFace()
	{
		GridLayout layout = new GridLayout(2, 1);
		
		this.setLayout(layout);
		setLayout(new BorderLayout(0, 0));
		this.add(aface, BorderLayout.CENTER);
		this.add(dface, BorderLayout.NORTH);
	}
	
	/* (non-Javadoc)
	 * @see gangwere.p7.IClockFace#setTime(int, int, int)
	 */
	@Override
	public void setTime(int hours, int minutes, int seconds) {
		// Pass these straight on to our Analog and Digital clockfaces.
		dface.setTime(hours, minutes, seconds);
		aface.setTime(hours, minutes, seconds);

	}

	/* (non-Javadoc)
	 * @see gangwere.p7.IClockFace#forceRefresh()
	 */
	@Override
	public void forceRefresh() {
		dface.forceRefresh();
		aface.forceRefresh();

	}

	/* (non-Javadoc)
	 * @see gangwere.p7.IClockFace#getContentPanel()
	 */
	@Override
	public JPanel getContentPanel() {
		// TODO Auto-generated method stub
		return this;
	}

}
