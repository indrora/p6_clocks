package gangwere.p6.faces;

import java.awt.Font;

import gangwere.p6.IClockFace;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;

public class HexFace extends JPanel implements IClockFace {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5319798720678313936L;
	JLabel display = new JLabel();
	
	public HexFace()
	{
		
		initGUI();
	}
	private void initGUI() {
		setBackground(Color.DARK_GRAY);
		setLayout(new BorderLayout(0, 0));
		display.setHorizontalAlignment(SwingConstants.CENTER);
		display.setBackground(Color.BLACK);
		display.setForeground(Color.GREEN);
		display.setText("0xFFFFFF");
		display.setFont(new Font("Consolas", Font.BOLD, 48));
		this.add(display);
	}
	
	@Override
	public void setTime(int hours, int minutes, int seconds) {
		// TODO Auto-generated method stub
		
		int wtf = ( hours << 16 ) + ( minutes << 8 ) + seconds;
		
		display.setText(String.format("0x%06x",wtf));
	}

	@Override
	public void forceRefresh() {
		display.repaint();

	}

	@Override
	public JPanel getContentPanel() {
		// TODO Auto-generated method stub
		return this;
	}

}
