package gangwere.p6.faces;

import java.awt.Font;
import java.awt.GridLayout;
import java.security.NoSuchAlgorithmException;

import gangwere.p6.IClockFace;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class HashFace extends JPanel implements IClockFace {

	/**
	 * Eclipse still loves you.
	 */
	private static final long serialVersionUID = -5174615195844328584L;
	JLabel hour_hash = new JLabel();
	JLabel min_hash = new JLabel();
	JLabel sec_hash = new JLabel();
	
	public HashFace()
	{
		this.setLayout(new GridLayout(3, 1));
		hour_hash.setVerticalAlignment(SwingConstants.BOTTOM);
		hour_hash.setHorizontalAlignment(SwingConstants.CENTER);
		hour_hash.setText("[stopped!]");
		hour_hash.setToolTipText("Kobold still hates you");
		this.add(hour_hash);
		min_hash.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(min_hash);
		sec_hash.setHorizontalAlignment(SwingConstants.CENTER);
		sec_hash.setVerticalAlignment(SwingConstants.TOP);
		this.add(sec_hash);
		Font consolas = new Font("Consolas", Font.BOLD, 12);
		hour_hash.setFont(consolas);
		min_hash.setFont(consolas);
		sec_hash.setFont(consolas);
	}
	
	@Override
	public void setTime(int hours, int minutes, int seconds) {
		try {
			
			hour_hash.setText(
					hashToString(IntToMD5(hours))+String.format("(%02d)", hours)
					);

			min_hash.setText(
					hashToString(IntToMD5(minutes))+String.format("(%02d)", minutes)
					);

			sec_hash.setText(
					hashToString(IntToMD5(seconds))+String.format("(%02d)", seconds)
					);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.out.println("Apparently, Your system does not have an MD5 digest. Oops!");
			System.exit(1);
		}
		
	}

	private static byte[] IntToMD5(int x) throws NoSuchAlgorithmException
	{
		java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
		return md.digest(new byte[] { (byte)x}); 
	}
	
	private static String hashToString(byte[] hash)
	{
		StringBuilder b = new StringBuilder();
		for(int i = 0; i < hash.length;i++)
		{
			b.append(String.format("%02X ", hash[i]));
		}
		return b.toString();
	}
	
	@Override
	public void forceRefresh() {
		repaint();

	}

	@Override
	public JPanel getContentPanel() {
		// TODO Auto-generated method stub
		return this;
	}

}
