package gangwere.p6.faces;

import gangwere.p6.IClockFace;

import javax.swing.JPanel;

public class AnalogFace extends JPanel implements IClockFace {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8492385592137456074L;
	StillClock c = new StillClock();
	
	public AnalogFace()
	{
		this.add(c);
		
	}
	
	@Override
	public void setTime(int hours, int minutes, int seconds) {
		// TODO Auto-generated method stub
		c.setHour(hours);
		c.setMinute(minutes);
		c.setSecond(seconds);
		
	}

	@Override
	public void forceRefresh() {
		c.repaint();
		
	}

	@Override
	public JPanel getContentPanel() {
		// TODO Auto-generated method stub
		return this;
	}

}
