package gangwere.p6;

import gangwere.p6.faces.AnalogFace;
import gangwere.p6.faces.CombinedFace;
import gangwere.p6.faces.HashFace;
import gangwere.p6.faces.HexFace;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;
import java.awt.event.InputEvent;

public class ClockWindow extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private MControlButtonPanel controlButtonPanel;
	private JMenuBar menuBar;
	private JMenu mnFile;
	private JMenu mnAddType;
	private JMenuItem mntmAnalog;
	private ClockManager clockManager;
	private JMenuItem mntmAnalogdigital;
	private JMenuItem mntmExit;
	private JMenuItem mntmHashClock;
	private JMenuItem mntmHexClock;
	private JMenu mnHelp;
	private JMenuItem mntmAbout;
	private JMenu mnClockTypes;
	private JMenuItem mntmAnalogdigital_1;
	private JMenuItem mntmAnalog_1;
	private JMenuItem mntmHashClock_1;
	private JMenuItem mntmHexClock_1;

	/**
	 * Create the frame.
	 */
	public ClockWindow() {
		initGUI();
		
		// We make sure we have a clock in the window.
		clockManager.addClock(new CombinedFace());
		// We make sure that the clock is stared by making the Start Listener on clockManager fire
		clockManager.getStartListener().actionPerformed(new ActionEvent(this, 0, ""));
		
		// Hook up all the connections for the Start and Stop buttons
		controlButtonPanel.addStartListener(clockManager.getStartListener());
		controlButtonPanel.addStopListener(clockManager.getStoplistener());
		
		
		// We done!
		
	}
	private void initGUI() {
		setTitle("Morgan Gangwere P6 Clocks");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 400);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		mnAddType = new JMenu("Add Type");
		mnFile.add(mnAddType);
		
		mntmAnalogdigital = new JMenuItem("Analog+Digital");
		mntmAnalogdigital.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));
		mntmAnalogdigital.addActionListener(new MntmAnalogdigitalActionListener());
		mnAddType.add(mntmAnalogdigital);
		
		mntmAnalog = new JMenuItem("Analog");
		mntmAnalog.addActionListener(new MntmAnalogActionListener());
		mnAddType.add(mntmAnalog);
		
		mntmHashClock = new JMenuItem("Hash Clock");
		mntmHashClock.addActionListener(new MntmHashClockActionListener());
		mnAddType.add(mntmHashClock);
		
		mntmHexClock = new JMenuItem("Hex Clock");
		mntmHexClock.addActionListener(new MntmHexClockActionListener());
		mnAddType.add(mntmHexClock);
		
		mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new MntmExitActionListener());
		mnFile.add(mntmExit);
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmAbout = new JMenuItem("About");
		mntmAbout.addActionListener(new MntmAboutActionListener());
		mnHelp.add(mntmAbout);
		
		mnClockTypes = new JMenu("Clock Types");
		mnHelp.add(mnClockTypes);
		
		mntmAnalogdigital_1 = new JMenuItem("CombinedFace");
		mntmAnalogdigital_1.addActionListener(new MntmAnalogdigital_1ActionListener());
		mnClockTypes.add(mntmAnalogdigital_1);
		
		mntmAnalog_1 = new JMenuItem("AnalogFace");
		mntmAnalog_1.addActionListener(new MntmAnalog_1ActionListener());
		mnClockTypes.add(mntmAnalog_1);
		
		mntmHashClock_1 = new JMenuItem("HashFace");
		mntmHashClock_1.addActionListener(new MntmHashClock_1ActionListener());
		mnClockTypes.add(mntmHashClock_1);
		
		mntmHexClock_1 = new JMenuItem("HexFace");
		mntmHexClock_1.addActionListener(new MntmHexClock_1ActionListener());
		mnClockTypes.add(mntmHexClock_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		controlButtonPanel = new MControlButtonPanel();
		contentPane.add(controlButtonPanel, BorderLayout.SOUTH);
		
		clockManager = new ClockManager();
		contentPane.add(clockManager, BorderLayout.CENTER);
	}

	private class MntmAnalogdigitalActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			clockManager.addClock(new CombinedFace());
		}
	}
	private class MntmAnalogActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			clockManager.addClock(new AnalogFace());
		}
	}
	private class MntmHashClockActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			clockManager.addClock(new HashFace());
		}
	}
	private class MntmHexClockActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			clockManager.addClock(new HexFace());
		}
	}
	private class MntmExitActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			System.exit(0);
		}
	}
	private class MntmAboutActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			JOptionPane.showMessageDialog(null,"Morgan Gangwere P6");
		}
	}
	private class MntmAnalogdigital_1ActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			JOptionPane.showMessageDialog(null, "CombinedFace shows both a Digital and an analog clock");
		}
	}
	private class MntmAnalog_1ActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			JOptionPane.showMessageDialog(null, "Displays an Analog clock face");
		}
	}
	private class MntmHashClock_1ActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			JOptionPane.showMessageDialog(null, "Displays the MD5 cryptograhic hashes of the hour, minute and second");
		}
	}
	private class MntmHexClock_1ActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			JOptionPane.showMessageDialog(null, "Displays the hour, minute and second as a combined hex value");
		}
	}
}
