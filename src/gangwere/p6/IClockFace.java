/**
 * 
 */
package gangwere.p6;

import javax.swing.JPanel;

/**
 * @author indrora
 *
 */
public interface IClockFace {
	
	void setTime(int hours, int minutes, int seconds);
	void forceRefresh();
	JPanel getContentPanel();
}
