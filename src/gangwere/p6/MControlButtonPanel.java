package gangwere.p6;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionEvent;

public class MControlButtonPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8031994976200918949L;
	
	
	private JButton btn_start;
	private JButton btn_stop;

	List<ActionListener> startListeners = new java.util.ArrayList<ActionListener>();

	List<ActionListener> stopListeners = new java.util.ArrayList<ActionListener>();
	
	/**
	 * Adds a listener for when the Start button is pressed
	 * @param l the listener
	 */
	public void addStartListener(ActionListener l)
	{
		startListeners.add(l);
	}
	/**
	 * Adds a listener for when the Stop button is pressed
	 * @param l the listener
	 */
	
	public void addStopListener(ActionListener l)
	{
		stopListeners.add(l);
	}
	
	
	/**
	 * Create the panel.
	 */
	public MControlButtonPanel() {

		initGUI();
	}
	private void initGUI() {
		
		btn_start = new JButton("Start");
		btn_start.addActionListener(new Btn_startActionListener());
		add(btn_start);
		
		btn_stop = new JButton("Stop");
		btn_stop.addActionListener(new Btn_stopActionListener());
		add(btn_stop);
	}

	private class Btn_startActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			for(int i=0;i<startListeners.size();i++)
			{
				startListeners.get(i).actionPerformed(e);
			}
		    btn_start.setBackground(Color.green);
		}
	}
	private class Btn_stopActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			for(int i=0;i<stopListeners.size();i++)
			{
				stopListeners.get(i).actionPerformed(e);
			}
		    btn_start.setBackground(Color.white);

		    btn_stop.setBackground(Color.RED);
		}
	}
}
