package gangwere.p6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JPanel;
import javax.swing.Timer;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.CardLayout;
import javax.swing.JToolBar;
import javax.swing.JLabel;

public class ClockManager extends JPanel {
	
	
	/**
	 * Wat
	 */
	private static final long serialVersionUID = 1L;

	ActionListener tickListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// Tick! Tick! Tick!
			// Update all our clock faces.
			for(int i = 0; i<faces.size();i++)
			{
				// We only care about the visible ones
				if(faces.get(i).getContentPanel().isVisible())
					forceClockUpdate(faces.get(i));
			}
			
		}
	};
	
	public void forceClockUpdate(IClockFace face)
	{
		   Calendar calendar = new GregorianCalendar();

		    // Set current hour, minute and second
		    int hours= calendar.get(Calendar.HOUR_OF_DAY);
		    int minutes = calendar.get(Calendar.MINUTE);
		    int seconds = calendar.get(Calendar.SECOND);
		    // And hand that off to our face
		    face.setTime(hours, minutes, seconds);
		    // And make sure it updates its state.
		    face.forceRefresh();
		    
	}
	
	Timer tick_timer = new Timer(200, tickListener);

	
	/**
	 * Stop action listener. Not public.
	 */
	static ActionListener stopListener = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			tick_timer.stop();
		}
	};
	
	/**
	 * Start action listener. Not public
	 */
	static ActionListener startListener = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tick_timer.start();
		}
	};
	
	/**
	 * Gets the ActionListener to invoke for starting the manager
	 * @return ActionListener for invocation.
	 */
	public ActionListener getStartListener() { return startListener; }
	
	/**
	 * Gets the ActionListener to invoke for stopping the manager
	 * @return ActionListner for invocation
	 */
	public ActionListener getStoplistener() {return stopListener; }
	
	ArrayList<IClockFace> faces = new ArrayList<IClockFace>();
	private JToolBar toolBar;
	private JButton prev_button;
	private JButton next_button;
	private JPanel face_panel;
	private JButton btnDestroy;
	private JLabel lblCardXOf;
	
	public void addClock(IClockFace face)
	{

		CardLayout lay = (CardLayout)(this.face_panel.getLayout());
		String id = String.format("face-%02d",faces.size()) ;
		this.face_panel.add(face.getContentPanel(), id );
		
		faces.add(face);
		

		// This makes sure the clock has a state. We don't assume any clock knows
		// what the hell is going on at any one time; that's not its job
		
		forceClockUpdate(face);

		lay.show(face_panel, id);
		
		updateCurrentCard();
	}
	
	public ClockManager() {
		
		initGUI();
	}
	
	/**
	 * Find and update the current visible card
	 */
	protected void updateCurrentCard()
	{
		
		// This is a serious hack.
		// We have to walk through each of our faces, finding the one thats visible
		// Since theoretically we can only have 1 visible item.
		// Java still hates you.
		
		for(int i=0;i<faces.size();i++)
		{
			IClockFace f = faces.get(i);
			if(f.getContentPanel().isVisible())
			{
				String lbl = String.format("Clock %d of %d (%s)",
						i+1, // Since its zero-indexed, +1
						faces.size(), // Not-zero-indexed
						f.getClass().getSimpleName() // Gets a "Simple" name (e.g. AnalogFace)
						);
				lblCardXOf.setText(lbl);
				break;
			}
		}
	}
	
	private void initGUI() {
		setLayout(new BorderLayout(0, 0));
		
		toolBar = new JToolBar();
		toolBar.setRollover(true);
		add(toolBar, BorderLayout.NORTH);
		
		prev_button = new JButton("Prev");
		prev_button.addActionListener(new Prev_buttonActionListener());
		prev_button.setIcon(null);
		toolBar.add(prev_button);
		
		next_button = new JButton("Next");
		next_button.addActionListener(new Next_buttonActionListener());
		next_button.setIcon(null);
		toolBar.add(next_button);
		
		btnDestroy = new JButton("Destroy");
		btnDestroy.addActionListener(new BtnDestroyActionListener());
		toolBar.add(btnDestroy);
		
		lblCardXOf = new JLabel("Clock X of Y");
		toolBar.add(lblCardXOf);
		
		face_panel = new JPanel();
		add(face_panel, BorderLayout.CENTER);
		face_panel.setLayout(new CardLayout(0, 0));
	}

	// *** DONT TOUCH THIS!!!! ***
	// These are the action listeners to mangle the panels
	
	private class Prev_buttonActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			CardLayout layout = (CardLayout)face_panel.getLayout();
			layout.previous(face_panel);
			tickListener.actionPerformed(new ActionEvent(this, 0, ""));
			updateCurrentCard();
		}
	}
	
	private class Next_buttonActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			CardLayout layout = (CardLayout)face_panel.getLayout();
			layout.next(face_panel);

			tickListener.actionPerformed(new ActionEvent(this, 0, ""));
			updateCurrentCard();
		}
	}
	
	/*
	 * 
	 * TOUCH THIS WITH CARE!
	 * 
	 * This mangles the cardLayout and the candleja-- 
	 * 
	 */
	
	private class BtnDestroyActionListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			
			// When using a CardLayout, we can be *fairly* certain that we will only be seeing one card at at time
			// So we can reasonably assume that once we hit the Visible face, we have found the one we want.
			
			// Iterate through all the cards.
			for(int i=0;i<faces.size();i++)
			{
				// Get the current IClockFace
				IClockFace f = faces.get(i);
				
				// If its visible, kill it.
				if(f.getContentPanel().isVisible())
				{
					face_panel.remove(f.getContentPanel());
					faces.remove(f);
					break;
				}
			}
			updateCurrentCard();
		}
	}
}
