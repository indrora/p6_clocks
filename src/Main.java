

import javax.swing.JFrame;
import javax.swing.UIManager;

import gangwere.p6.ClockWindow;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// I love this game.
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception err)
		{
			System.out.println("I couldn't set the system style! OH NO");
			System.exit(1);
		}
		
		ClockWindow w = new ClockWindow();
		w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		w.setVisible(true);
	}

}
