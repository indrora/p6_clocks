What?
=====

This is a simple project I wrote back when I was taking a Java class. It wasn't one of my favorites, but I didn't mind writing it.

Its a multi-faced clock. Some of my classmates did fun things like changed the face of the clock, etc -- I decided to go a much more generic route: there are IClockFace[s] which simply implement everything a JFrame needs to and a few functions (namely, it has to update its time properly). There are some things I could have done, but the project got a B, so I'm happy.

Why a B?
========

The prof didn't like one little detail of my project: The original specification stated that there needed to be "A description of things" -- Namely, she wanted About-boxes for everything.

Not wanting to add another thing to my IClockFace, I simply statically added a list of helpers. Ugly, but it works. I wanted to have IDescribable, where it had a single function, `describe()` which told you what you were looking at. Didn't have time, didn't bother.

Fun things to learn from this code
==================================

I learned quite a bit from writing this. Here's some of the take-homes:

 1. Be flexible -- and learn how to see down the road. There are things I really like about IClockFace -- it was the second interface I'd written for this class (I was well advanced in C# by the time i got there, so I'm not a stranger to this) but I learned how other people attacked this problem. One person went so far as to learn how Introspection works so they could just find a function.
 2. Be reusable -- See `gangwere.p6.MControlButtonPanel` for an example here. Other implementations had the parent window reciving the events, which would then propogate up into the clock face. I found this inelegant and un-reusable. This was my response.
 3. Know how to implement singletons. Singletons can be finnicky beasts if not done right, and this was my third singleton in the class. I wasn't entirely certain how `static ActionListener` would work -- I came to find out that this was a really good way to do it.
 4. Think ahead -- I designed this so that just about anything could update itself. At one point, I toyed with a GL clock I had found. There's a subtle reason why `gangwere.p6.ClockManager` ticks every 200ms (approx 5fps).
 5. OH GOD THE BEES.

License
=======

This code is placed under the IDGAFPL. Its full text is below:

    The I Don't Give A Fuck Public License (version 1, Oct 1, 2012):

    Article 1:
       I don't give a fuck what you do with it; treat it like Public Domain.
    Article 2:
       If you do something and it breaks, makes you loose your house, or makes your neighbor angry, I don't give a fuck.


